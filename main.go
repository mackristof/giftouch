/*
 * Copyright (C) 2020  MacKristof
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package main

import (
	"log"

	"github.com/nanu-c/qml-go"
)

func main() {
	err := qml.Run(run)
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	engine := qml.NewEngine()
	component, err := engine.LoadFile("qml/Main.qml")
	if err != nil {
		return err
	}

	searchStruct := SearchStruct{Term: "test", Url: make([]string, 0), Len: 0}
	context := engine.Context()
	context.SetVar("searchStruct", &searchStruct)
	//testvar.GetMessage()

	win := component.CreateWindow(nil)
	searchStruct.Root = win.Root()
	win.Show()
	win.Wait()

	return nil
}

type SearchStruct struct {
	Root qml.Object
	Term string
	Url  []string
	Len  int
}

func (searchStruct *SearchStruct) Search() {
	//go func() {
	println(searchStruct.Term)
	searchStruct.Url = append(searchStruct.Url, "https://media0.giphy.com/media/7MZ0v9KynmiSA/giphy.gif?cid=ccfcc112d4dc7f5e873b8544c63c4b5e2e04b22621e2349b&rid=giphy.gif")
	searchStruct.Len = len(searchStruct.Url)
	qml.Changed(searchStruct, &searchStruct.Len)
	//}()
}

func (searchStruct SearchStruct) Get(index int) string {
	return searchStruct.Url[index]
}
